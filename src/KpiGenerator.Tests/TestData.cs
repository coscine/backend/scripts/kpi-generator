using Coscine.ApiClient.Core.Model;

static class TestData
{
    public static List<ProjectAdminDto> ProjectAdminDtos => ReturnProjectAdminDtos();
    public static List<ResourceAdminDto> ResourceAdminDtos => ReturnResourceAdminDtos();
    public static List<UserDto> UserDtos => ReturnUserDtos();
    public static List<RoleDto> RoleDtos => ReturnRoleDtos();


    private static List<RoleDto> ReturnRoleDtos()
    {
        return
        [
            new(id: Guid.Parse("778f7bd2-128b-45aa-a093-807da3b9aecc"),
                displayName: "Role One",
                description: "Role One Description"
            ),
            new(id: Guid.Parse("6e5aadc3-72ea-4f96-827d-90c5c15d34af"),
                displayName: "Role Two",
                description: "Role Two Description"
            )
        ];
    }

    private static List<UserDto> ReturnUserDtos()
    {
        return
        [
            new(id: Guid.Parse("fd971cef-7c02-45df-97c4-ef757c05bc19"),
                displayName: "User One",
                givenName: "User",
                familyName: "One",
                emails: [ new(email: "one@user.com", isConfirmed: true, isPrimary: true) ],
                language: new(id: Guid.NewGuid(), displayName: "English", abbreviation: "en"),
                areToSAccepted: true,
                latestActivity: DateTime.UtcNow.AddHours(-1),
                disciplines: [new(id: Guid.NewGuid(), uri: "https://one.discipline.org", displayNameEn: "Discipline One", displayNameDe: "Disziplin Eins")],
                organizations: [new(uri: "https://ror.org/12345", displayName: "Organization One", readOnly: true)],
                identities: [new(id: Guid.NewGuid(), displayName: "Provider One")]
            ),
            new(id: Guid.Parse("21c95165-8b44-46a5-b711-4d4e0c54eda2"),
                displayName: "User Two",
                givenName: "User",
                familyName: "Two",
                emails: [ new(email: "two@user.com", isConfirmed: true, isPrimary: true) ],
                language: new(id: Guid.NewGuid(), displayName: "English", abbreviation: "en"),
                areToSAccepted: true,
                latestActivity: DateTime.UtcNow.AddHours(-3),
                disciplines: [new(id: Guid.NewGuid(), uri: "https://two.discipline.org", displayNameEn: "Discipline Two", displayNameDe: "Disziplin Zwei")],
                organizations: [new(uri: "https://ror.org/54321", displayName: "Organization Two", readOnly: true)],
                identities: [new(id: Guid.NewGuid(), displayName: "Provider Two")]
            )
        ];
    }

    private static List<ResourceAdminDto> ReturnResourceAdminDtos()
    {
        return
        [
            new(id: Guid.Parse("a5092027-0d0b-4be6-9025-5a2397f34fce"),
                name: "Resource One",
                displayName: "Resource One",
                description: "Resource One Description",
                pid: "resource-one",
                type: new(generalType: "ds", specificType: "dsweb"),
                dateCreated: DateTime.UtcNow,
                creator: new() { Id = Guid.NewGuid() },
                resourceQuota: new(resource: new(id: Guid.Parse("a5092027-0d0b-4be6-9025-5a2397f34fce")), usedPercentage: 0.5f, used: new(), reserved: new()),
                visibility: new(),
                keywords: ["keyword1", "keyword2"],
                disciplines: [new(id: Guid.NewGuid(), uri: "https://one.discipline.org", displayNameEn: "Discipline One", displayNameDe: "Disziplin Eins")],
                license: new(),
                usageRights: "usage rights",
                metadataLocalCopy: false,
                metadataExtraction: false,
                archived: false,
                projects: [],
                applicationProfile: new(uri: "https://example.org"),
                fixedValues: [],
                projectResources: [new(projectId: Guid.Parse("0fd58e44-f3cc-4aec-a865-b8f37aa17466"), resourceId: Guid.Parse("a5092027-0d0b-4be6-9025-5a2397f34fce"))],
                deleted: false
            ),
            new(id: Guid.Parse("cc5d1783-8ff1-4401-a2ce-d1300bd1b2a5"),
                name: "Resource Two",
                displayName: "Resource Two",
                description: "Resource Two Description",
                pid: "resource-two",
                type: new(generalType: "ds", specificType: "dss3"),
                dateCreated: DateTime.UtcNow,
                creator: new() { Id = Guid.NewGuid() },
                resourceQuota: new(resource: new(id: Guid.Parse("cc5d1783-8ff1-4401-a2ce-d1300bd1b2a5")), usedPercentage: 0.5f, used: new(), reserved: new()),
                visibility: new(),
                keywords: ["keyword3"],
                disciplines: [new(id: Guid.NewGuid(), uri: "https://one.discipline.org", displayNameEn: "Discipline One", displayNameDe: "Disziplin Eins")],
                license: new(),
                usageRights: "usage rights",
                metadataLocalCopy: false,
                metadataExtraction: false,
                archived: false,
                projects: [],
                applicationProfile: new(uri: "https://example.org"),
                fixedValues: [],
                projectResources: [new(projectId: Guid.Parse("0fd58e44-f3cc-4aec-a865-b8f37aa17466"), resourceId: Guid.Parse("a5092027-0d0b-4be6-9025-5a2397f34fce"))],
                deleted: false
            )
        ];
    }

    private static List<ProjectAdminDto> ReturnProjectAdminDtos()
    {
        return
        [
            new(id: Guid.Parse("ce1af24e-2719-425c-947d-d7538f06fed0"),
                name: "Project One",
                displayName: "Project One",
                description: "Project One Description",
                pid: "project-one",
                creationDate: DateTime.UtcNow,
                slug: "project-one",
                creator: new() { Id = Guid.NewGuid() },
                projectQuota: [],
                visibility: new(),
                disciplines: [new(id: Guid.NewGuid(), uri: "https://one.discipline.org", displayNameEn: "Discipline One", displayNameDe: "Disziplin Eins")],
                organizations: [
                    new(uri: "https://ror.org/12345", displayName: "Organization One", responsible: true),
                    new(uri: "https://ror.org/54321", displayName: "Organization Two", responsible: false)
                ],
                projectRoles: [ new(projectId: Guid.Parse("ce1af24e-2719-425c-947d-d7538f06fed0"), userId: Guid.Parse("fd971cef-7c02-45df-97c4-ef757c05bc19"), roleId: Guid.Parse("778f7bd2-128b-45aa-a093-807da3b9aecc")) ]
            ),
            new(id: Guid.Parse("0fd58e44-f3cc-4aec-a865-b8f37aa17466"),
                name: "Project Two",
                displayName: "Project Two",
                description: "Project Two Description",
                pid: "project-two",
                creationDate: DateTime.UtcNow,
                slug: "project-two",
                creator: new() { Id = Guid.NewGuid() },
                projectQuota: [],
                visibility: new(),
                disciplines: [new(id: Guid.NewGuid(), uri: "https://one.discipline.org", displayNameEn: "Discipline One", displayNameDe: "Disziplin Eins")],
                organizations: [
                    new(uri: "https://ror.org/12345", displayName: "Organization One", responsible: false),
                    new(uri: "https://ror.org/54321", displayName: "Organization Two", responsible: true)
                ],
                projectRoles: [ new(projectId: Guid.Parse("0fd58e44-f3cc-4aec-a865-b8f37aa17466"), userId: Guid.Parse("21c95165-8b44-46a5-b711-4d4e0c54eda2"), roleId: Guid.Parse("6e5aadc3-72ea-4f96-827d-90c5c15d34af")) ]
            )
        ];
    }
}