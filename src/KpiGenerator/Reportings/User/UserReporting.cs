﻿using AutoMapper;
using Coscine.ApiClient;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Model;
using Coscine.KpiGenerator.Models;
using Coscine.KpiGenerator.Models.ConfigurationModels;
using Coscine.KpiGenerator.Utils;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using static KPIGenerator.Utils.CommandLineOptions;

namespace Coscine.KpiGenerator.Reportings.User;

public class UserReporting
{
    private readonly IMapper _mapper;
    private readonly ILogger<UserReporting> _logger;
    private readonly IStorageService _gitlabStorageService;
    private readonly IStorageService _localStorageService;
    private readonly KpiConfiguration _kpiConfiguration;
    private readonly ReportingConfiguration _reportingConfiguration;
    private readonly IAdminApi _adminApi;
    private readonly IRoleApi _roleApi;

    public UserReportingOptions Options { get; private set; } = null!;
    public string ReportingFileName { get; }

    public UserReporting(
        IMapper mapper,
        ILogger<UserReporting> logger,
        [FromKeyedServices("gitlab")] IStorageService gitlabStorageService,
        [FromKeyedServices("local")] IStorageService localStorageService,
        IOptionsMonitor<KpiConfiguration> kpiConfiguration,
        IOptionsMonitor<ReportingConfiguration> reportingConfiguration,
        IAdminApi adminApi,
        IRoleApi roleApi
    )
    {
        _mapper = mapper;
        _logger = logger;
        _gitlabStorageService = gitlabStorageService;
        _localStorageService = localStorageService;
        _kpiConfiguration = kpiConfiguration.CurrentValue;
        _reportingConfiguration = reportingConfiguration.CurrentValue;
        ReportingFileName = _kpiConfiguration.UserKpi.FileName;

        _adminApi = adminApi;
        _roleApi = roleApi;
    }

    public async Task<bool> RunAsync(UserReportingOptions reportingOptions)
    {
        _logger.LogInformation("Generating User Reporting...");
        Options = reportingOptions;
        var reportingFiles = await GenerateReportingAsync();

        bool success = false;
        _logger.LogInformation("Publishing to GitLab...");
        // Publish to GitLab first, if that fails, publish to local storage
        try
        {
            if (!Options.DummyMode)
            {
                success = await _gitlabStorageService.PublishAsync("User Reporting", reportingFiles);
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "GitLab publish failed. Falling back to local storage.");
        }

        if (!success || Options.DummyMode)
        {
            _logger.LogInformation("Publishing to local storage instead.");
            success = await _localStorageService.PublishAsync("User Reporting", reportingFiles);
        }

        return success;
    }

    public virtual async Task<IEnumerable<ReportingFileObject>> GenerateReportingAsync()
    {
        _logger.LogDebug("Getting all projects...");
        var projects = await PaginationHelper.GetAllAsync<ProjectAdminDtoPagedResponse, ProjectAdminDto>(
                (currentPage) => _adminApi.GetAllProjectsAsync(includeDeleted: false, pageNumber: currentPage, pageSize: 50)).ToListAsync();
        _logger.LogDebug("Got all projects.");

        _logger.LogDebug("Getting all roles...");
        var roles = await PaginationHelper.GetAllAsync<RoleDtoPagedResponse, RoleDto>(
            (currentPage) => _roleApi.GetRolesAsync(pageNumber: currentPage, pageSize: 50)).ToListAsync();
        _logger.LogDebug("Got all roles.");

        _logger.LogDebug("Working on users asynchronously...");
        var users = PaginationHelper.GetAllAsync<UserDtoPagedResponse, UserDto>(
            (currentPage) => _adminApi.GetAllUsersAsync(tosAccepted: true, pageNumber: currentPage, pageSize: 50));

        var reportingFiles = new List<ReportingFileObject>();
        var returnObjects = new List<UserReport>();

        // Do additional processing
        await foreach (var user in users)
        {
            _logger.LogDebug("Processing user {user}", user.Id);
            var userForReport = _mapper.Map<UserReport>(user);
            var relatedProjects = new List<RelatedProject>();

            // Set the user project roles from the related projects
            var relatedProjectDtos = projects?.Where(p => p.ProjectRoles.Any(pr => pr.UserId == user.Id)) ?? [];
            _logger.LogDebug("Processing related projects...");
            foreach (var relatedProject in relatedProjectDtos)
            {
                var roleId = relatedProject.ProjectRoles.FirstOrDefault(pr => pr.UserId == user.Id)?.RoleId;
                var role = roles.FirstOrDefault(r => r.Id == roleId);
                _logger.LogDebug("Processed role {roleId}...", roleId);
                if (role is not null)
                {
                    relatedProjects.Add(new()
                    {
                        ProjectId = relatedProject.Id.ToString(),
                        Role = role.DisplayName,
                    });
                }
            }
            _logger.LogDebug("Processed related projects...");

            userForReport.RelatedProjects = relatedProjects;
            returnObjects.Add(userForReport);
        }
        _logger.LogInformation("{n} users mapped.", returnObjects.Count);

        // General File
        reportingFiles.Add(new ReportingFileObject
        {
            Path = Helpers.GetReportingPathGeneral(ReportingFileName),
            Content = Helpers.ConvertStringContentsToStream(JsonConvert.SerializeObject(returnObjects, Formatting.Indented)),
        });
        _logger.LogInformation("General file generated.");

        // Per Organization
        reportingFiles.AddRange(GeneratePerOrganization(returnObjects));
        _logger.LogInformation("Per Organization files generated.");

        return reportingFiles;
    }

    private List<ReportingFileObject> GeneratePerOrganization(List<UserReport> returnObjects)
    {
        var reportingFilesPerOrganization = new List<ReportingFileObject>();
        var organizationsFromProjects = Helpers.GetTopLevelOrganizationsFromEntries(returnObjects.SelectMany(ro => ro.Organizations));
        foreach (var entry in organizationsFromProjects)
        {
            // Handle the case where the ROR URL is the default one
            var rorUrl = entry.RorUrl.Equals("https://ror.org/", StringComparison.InvariantCultureIgnoreCase) ? _reportingConfiguration.Organization?.OtherOrganization?.RorUrl ?? "_other" : entry.RorUrl;

            var returnObjectsForOrganization = returnObjects.Where(ro => ro.Organizations.Select(o => o.RorUrl).Any(e => e is not null && e.Contains(rorUrl))).ToList();
            if (returnObjectsForOrganization.Count != 0)
            {
                var reportingFile = new ReportingFileObject
                {
                    Path = Helpers.GetReportingPathOrganization(rorUrl, ReportingFileName),
                    Content = Helpers.ConvertStringContentsToStream(JsonConvert.SerializeObject(returnObjectsForOrganization, Formatting.Indented))
                };
                reportingFilesPerOrganization.Add(reportingFile);
            }
        }

        return reportingFilesPerOrganization;
    }
}