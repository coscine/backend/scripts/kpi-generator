﻿using CommandLine;

namespace KPIGenerator.Utils;

public static partial class CommandLineOptions
{
    [Verb("users", HelpText = "Generate user KPIs")]
    public class UserReportingOptions : BaseOptions
    {
        // Add verb specific options here
    }
}