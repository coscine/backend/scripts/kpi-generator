﻿using AutoMapper;
using Coscine.ApiClient;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;
using Coscine.KpiGenerator.Models;
using Coscine.KpiGenerator.Models.ConfigurationModels;
using Coscine.KpiGenerator.Utils;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using static KPIGenerator.Utils.CommandLineOptions;

namespace Coscine.KpiGenerator.Reportings.Resource;

public class ResourceReporting
{
    private readonly IMapper _mapper;
    private readonly ILogger<ResourceReporting> _logger;
    private readonly IStorageService _gitlabStorageService;
    private readonly IStorageService _localStorageService;
    private readonly KpiConfiguration _kpiConfiguration;
    private readonly ReportingConfiguration _reportingConfiguration;
    private readonly IAdminApi _adminApi;

    public ResourceReportingOptions Options { get; private set; } = null!;
    public string ReportingFileName { get; }

    public ResourceReporting(
        IMapper mapper,
        ILogger<ResourceReporting> logger,
        [FromKeyedServices("gitlab")] IStorageService gitlabStorageService,
        [FromKeyedServices("local")] IStorageService localStorageService,
        IOptionsMonitor<KpiConfiguration> kpiConfiguration,
        IOptionsMonitor<ReportingConfiguration> reportingConfiguration,
        IAdminApi adminApi
    )
    {
        _mapper = mapper;
        _logger = logger;
        _gitlabStorageService = gitlabStorageService;
        _localStorageService = localStorageService;
        _kpiConfiguration = kpiConfiguration.CurrentValue;
        _reportingConfiguration = reportingConfiguration.CurrentValue;
        ReportingFileName = _kpiConfiguration.ResourceKpi.FileName;

        var configuration = new Configuration()
        {
            BasePath = $"{_reportingConfiguration.Endpoint.TrimEnd('/')}/coscine",
            ApiKeyPrefix = { { "Authorization", "Bearer" } },
            ApiKey = { { "Authorization", _reportingConfiguration.ApiKey } },
            Timeout = 300000 // 5 minutes
        };

        _adminApi = adminApi;
    }

    public async Task<bool> RunAsync(ResourceReportingOptions reportingOptions)
    {
        _logger.LogInformation("Generating Resource Reporting...");
        Options = reportingOptions;
        var reportingFiles = await GenerateReportingAsync();

        bool success = false;
        _logger.LogInformation("Publishing to GitLab...");
        // Publish to GitLab first, if that fails, publish to local storage
        try
        {
            if (!Options.DummyMode)
            {
                success = await _gitlabStorageService.PublishAsync("Resource Reporting", reportingFiles);
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "GitLab publish failed. Falling back to local storage.");
        }

        if (!success || Options.DummyMode)
        {
            _logger.LogInformation("Publishing to local storage instead.");
            success = await _localStorageService.PublishAsync("Resource Reporting", reportingFiles);
        }

        return success;
    }

    public virtual async Task<IEnumerable<ReportingFileObject>> GenerateReportingAsync()
    {
        _logger.LogDebug("Getting all projects...");
        var projects = await PaginationHelper.GetAllAsync<ProjectAdminDtoPagedResponse, ProjectAdminDto>(
                (currentPage) => _adminApi.GetAllProjectsAsync(includeDeleted: true, pageNumber: currentPage, pageSize: 50)).ToListAsync();
        _logger.LogDebug("Got all projects.");

        _logger.LogDebug("Working on resources asynchronously...");
        var resources = PaginationHelper.GetAllAsync<ResourceAdminDtoPagedResponse, ResourceAdminDto>(
                (currentPage) => _adminApi.GetAllResourcesAsync(includeDeleted: false, includeQuotas: true, pageNumber: currentPage, pageSize: 50));

        var reportingFiles = new List<ReportingFileObject>();
        var returnObjects = new List<ResourceReport>();

        // Do additional processing
        await foreach (var resource in resources)
        {
            _logger.LogDebug("Processing resource {resourceId}...", resource.Id);
            var returnObject = _mapper.Map<ResourceReport>(resource);
            // Set the resource organization from the related project
            var project = projects.FirstOrDefault(p => p.Id.ToString() == returnObject.RelatedProjectId);
            _logger.LogDebug("Processed related project {projectId}...", project?.Id);
            if (project != null)
            {
                var o = _mapper.Map<List<ProjectOrganization>>(project?.Organizations);
                returnObject.Organizations = o ?? [];
            }
            _logger.LogDebug("Processed organizations...");
            returnObjects.Add(returnObject);
        }
        _logger.LogInformation("{n} resources mapped.", returnObjects.Count);

        // General File
        reportingFiles.Add(new ReportingFileObject
        {
            Path = Helpers.GetReportingPathGeneral(ReportingFileName),
            Content = Helpers.ConvertStringContentsToStream(JsonConvert.SerializeObject(returnObjects, Formatting.Indented)),
        });
        _logger.LogInformation("General file generated.");

        // Per Organization
        reportingFiles.AddRange(GeneratePerOrganization(returnObjects));
        _logger.LogInformation("Per Organization files generated.");

        return reportingFiles;
    }

    private List<ReportingFileObject> GeneratePerOrganization(List<ResourceReport> returnObjects)
    {
        var reportingFilesPerOrganization = new List<ReportingFileObject>();
        var organizationsFromProjects = Helpers.GetTopLevelOrganizationsFromEntries(returnObjects.SelectMany(ro => ro.Organizations));
        foreach (var entry in organizationsFromProjects)
        {
            // Handle the case where the ROR URL is the default one
            var rorUrl = entry.RorUrl.Equals("https://ror.org/", StringComparison.InvariantCultureIgnoreCase) ? _reportingConfiguration.Organization?.OtherOrganization?.RorUrl ?? "_other" : entry.RorUrl;

            var returnObjectsForOrganization = returnObjects.Where(ro => ro.Organizations.Select(o => o.RorUrl).Any(e => e.Contains(rorUrl))).ToList();
            if (returnObjectsForOrganization.Count != 0)
            {
                var reportingFile = new ReportingFileObject
                {
                    Path = Helpers.GetReportingPathOrganization(rorUrl, ReportingFileName),
                    Content = Helpers.ConvertStringContentsToStream(JsonConvert.SerializeObject(returnObjectsForOrganization, Formatting.Indented))
                };
                reportingFilesPerOrganization.Add(reportingFile);
            }
        }

        return reportingFilesPerOrganization;
    }
}