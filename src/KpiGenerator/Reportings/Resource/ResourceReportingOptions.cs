﻿using CommandLine;

namespace KPIGenerator.Utils;

public static partial class CommandLineOptions
{
    [Verb("resources", HelpText = "Generate resource KPIs")]
    public class ResourceReportingOptions : BaseOptions
    {
        // Add verb specific options here
    }
}