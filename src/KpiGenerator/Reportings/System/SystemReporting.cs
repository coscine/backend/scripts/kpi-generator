﻿using Coscine.KpiGenerator.Models;
using Coscine.KpiGenerator.Models.ConfigurationModels;
using Coscine.KpiGenerator.Utils;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;
using static KPIGenerator.Utils.CommandLineOptions;
using HttpMethod = System.Net.Http.HttpMethod;

namespace Coscine.KpiGenerator.Reportings.System;

public class SystemReporting
{
    private readonly ILogger<SystemReporting> _logger;
    private readonly IStorageService _gitlabStorageService;
    private readonly IStorageService _localStorageService;
    private readonly KpiConfiguration _kpiConfiguration;
    private readonly ReportingConfiguration _reportingConfiguration;
    private readonly HttpClient _httpClient;

    public SystemReportingOptions Options { get; private set; } = null!;
    public string ReportingFileName { get; }

    public SystemReporting(
        IHttpClientFactory httpClientFactory,
        ILogger<SystemReporting> logger,
        [FromKeyedServices("gitlab")] IStorageService gitlabStorageService,
        [FromKeyedServices("local")] IStorageService localStorageService,
        IOptionsMonitor<KpiConfiguration> kpiConfiguration,
        IOptionsMonitor<ReportingConfiguration> reportingConfiguration
    )
    {
        _logger = logger;
        _gitlabStorageService = gitlabStorageService;
        _localStorageService = localStorageService;
        _kpiConfiguration = kpiConfiguration.CurrentValue;
        _httpClient = httpClientFactory.CreateClient("MaintenanceClient");
        _reportingConfiguration = reportingConfiguration.CurrentValue;
        ReportingFileName = _kpiConfiguration.SystemKpi.FileName;
    }

    public async Task<bool> RunAsync(SystemReportingOptions reportingOptions)
    {
        _logger.LogInformation("Generating System Reporting...");
        Options = reportingOptions;
        var reportingFiles = await GenerateReportingAsync();

        bool success = false;
        _logger.LogInformation("Publishing to GitLab...");
        // Publish to GitLab first, if that fails, publish to local storage
        try
        {
            if (!Options.DummyMode)
            {
                success = await _gitlabStorageService.PublishAsync("System Reporting", reportingFiles);
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "GitLab publish failed. Falling back to local storage.");
        }

        if (!success || Options.DummyMode)
        {
            _logger.LogInformation("Publishing to local storage instead.");
            success = await _localStorageService.PublishAsync("System Reporting", reportingFiles);
        }

        return success;
    }

    public async Task<IEnumerable<ReportingFileObject>> GenerateReportingAsync()
    {
        var reportingFiles = new List<ReportingFileObject>();
        var returnObject = new SystemReport()
        {
            Banners = await GetMaintenanceBannersAsync(),
        };
        _logger.LogInformation("{n} relevant banners found.", returnObject.Banners?.Count);

        // General File
        reportingFiles.Add(new ReportingFileObject
        {
            Path = Helpers.GetReportingPathGeneral(ReportingFileName),
            Content = Helpers.ConvertStringContentsToStream(JsonConvert.SerializeObject(returnObject, Formatting.Indented)),
        });
        _logger.LogInformation("General file generated.");

        // No per organization file

        return reportingFiles;
    }

    private async Task<List<MaintenanceReport>?> GetMaintenanceBannersAsync()
    {
        var requestMessage = new HttpRequestMessage()
        {
            RequestUri = new Uri(_kpiConfiguration.SystemKpi.Maintenance.Url),
            Method = HttpMethod.Get, // Use global alias
        };

        // Add Basic Authentication Headers
        var authenticationString = $"{_kpiConfiguration.SystemKpi.Maintenance.Username}:{_kpiConfiguration.SystemKpi.Maintenance.Password}";
        var base64EncodedAuthenticationString = Convert.ToBase64String(Encoding.ASCII.GetBytes(authenticationString));
        requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);

        var result = await _httpClient.SendAsync(requestMessage);
        var responseBody = JsonConvert.DeserializeObject<List<MaintenanceReport>>(result.Content.ReadAsStringAsync().Result);
        if (responseBody is null)
        {
            return null;
        }
        return [.. responseBody.OrderBy(e => e.StartAt)];
    }
}