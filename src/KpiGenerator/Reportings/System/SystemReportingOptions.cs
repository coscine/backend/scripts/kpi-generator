﻿using CommandLine;

namespace KPIGenerator.Utils;

public static partial class CommandLineOptions
{
    [Verb("system", HelpText = "Generate system status KPIs")]
    public class SystemReportingOptions : BaseOptions
    {
        // Add verb specific options here
    }
}