﻿using CommandLine;

namespace KPIGenerator.Utils;

public static partial class CommandLineOptions
{
    [Verb("projects", HelpText = "Generate project KPIs")]
    public class ProjectReportingOptions : BaseOptions
    {
        // Add verb specific options here
    }
}