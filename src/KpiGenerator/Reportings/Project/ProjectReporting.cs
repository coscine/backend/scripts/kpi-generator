﻿using AutoMapper;
using Coscine.ApiClient;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Model;
using Coscine.KpiGenerator.Models;
using Coscine.KpiGenerator.Models.ConfigurationModels;
using Coscine.KpiGenerator.Utils;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using static KPIGenerator.Utils.CommandLineOptions;

namespace Coscine.KpiGenerator.Reportings.Project;

public class ProjectReporting
{
    private readonly IMapper _mapper;
    private readonly ILogger<ProjectReporting> _logger;
    private readonly IStorageService _gitlabStorageService;
    private readonly IStorageService _localStorageService;
    private readonly KpiConfiguration _kpiConfiguration;
    private readonly ReportingConfiguration _reportingConfiguration;
    private readonly IAdminApi _adminApi;
    private readonly IProjectQuotaApi _projectQuotaApi;

    public ProjectReportingOptions Options { get; private set; } = null!;
    public string ReportingFileName { get; }

    public ProjectReporting(
        IMapper mapper,
        ILogger<ProjectReporting> logger,
        [FromKeyedServices("gitlab")] IStorageService gitlabStorageService,
        [FromKeyedServices("local")] IStorageService localStorageService,
        IOptionsMonitor<KpiConfiguration> kpiConfiguration,
        IOptionsMonitor<ReportingConfiguration> reportingConfiguration,
        IAdminApi adminApi,
        IProjectQuotaApi projectQuotaApi
    )
    {
        _mapper = mapper;
        _logger = logger;
        _gitlabStorageService = gitlabStorageService;
        _localStorageService = localStorageService;
        _kpiConfiguration = kpiConfiguration.CurrentValue;
        _reportingConfiguration = reportingConfiguration.CurrentValue;
        ReportingFileName = _kpiConfiguration.ProjectKpi.FileName;

        _adminApi = adminApi;
        _projectQuotaApi = projectQuotaApi;
    }

    public async Task<bool> RunAsync(ProjectReportingOptions reportingOptions)
    {
        _logger.LogInformation("Generating Project Reporting...");
        Options = reportingOptions;

        var reportingFiles = await GenerateReportingAsync();

        bool success = false;
        _logger.LogInformation("Publishing to GitLab...");
        // Publish to GitLab first, if that fails, publish to local storage
        try
        {
            if (!Options.DummyMode)
            {
                success = await _gitlabStorageService.PublishAsync("Project Reporting", reportingFiles);
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "GitLab publish failed. Falling back to local storage.");
        }

        if (!success || Options.DummyMode)
        {
            _logger.LogInformation("Publishing to local storage instead.");
            success = await _localStorageService.PublishAsync("Project Reporting", reportingFiles);
        }

        return success;
    }

    public virtual async Task<IEnumerable<ReportingFileObject>> GenerateReportingAsync()
    {
        _logger.LogDebug("Working on projects asynchronously...");
        var projects = PaginationHelper.GetAllAsync<ProjectAdminDtoPagedResponse, ProjectAdminDto>(
                (currentPage) => _adminApi.GetAllProjectsAsync(includeDeleted: false, includeQuotas: true, includePublicationRequests: true, pageNumber: currentPage, pageSize: 50));

        var reportingFiles = new List<ReportingFileObject>();
        var returnObjects = new List<ProjectReport>();

        // Additional processing
        await foreach (var project in projects)
        {
            _logger.LogDebug("Processing project {projectId}...", project.Id);
            var returnObject = _mapper.Map<ProjectReport>(project);
            returnObjects.Add(returnObject);
        }
        _logger.LogInformation("{n} projects mapped.", returnObjects.Count);

        // General File
        reportingFiles.Add(new ReportingFileObject
        {
            Path = Helpers.GetReportingPathGeneral(ReportingFileName),
            Content = Helpers.ConvertStringContentsToStream(JsonConvert.SerializeObject(returnObjects, Formatting.Indented)),
        });
        _logger.LogInformation("General file generated.");

        // Per Organization
        reportingFiles.AddRange(GeneratePerOrganization(returnObjects));
        _logger.LogInformation("Per Organization files generated.");

        return reportingFiles;
    }

    private List<ReportingFileObject> GeneratePerOrganization(List<ProjectReport> returnObjects)
    {
        var reportingFilesPerOrganization = new List<ReportingFileObject>();
        var organizationsFromProjects = Helpers.GetTopLevelOrganizationsFromEntries(returnObjects.SelectMany(pr => pr.Organizations));
        foreach (var entry in organizationsFromProjects)
        {
            // Handle the case where the ROR URL is the default one
            var rorUrl = entry.RorUrl.Equals("https://ror.org/", StringComparison.InvariantCultureIgnoreCase) ? _reportingConfiguration.Organization?.OtherOrganization?.RorUrl ?? "_other" : entry.RorUrl;

            var returnObjectsForOrganization = returnObjects.Where(ro => ro.Organizations.Select(o => o.RorUrl).Any(e => e.Contains(rorUrl))).ToList();
            if (returnObjectsForOrganization.Count != 0)
            {
                var reportingFile = new ReportingFileObject
                {
                    Path = Helpers.GetReportingPathOrganization(rorUrl, ReportingFileName),
                    Content = Helpers.ConvertStringContentsToStream(JsonConvert.SerializeObject(returnObjectsForOrganization, Formatting.Indented))
                };
                reportingFilesPerOrganization.Add(reportingFile);
            }
        }

        return reportingFilesPerOrganization;
    }
}