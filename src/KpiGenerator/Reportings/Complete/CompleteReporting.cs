﻿using Coscine.KpiGenerator.Models;
using Coscine.KpiGenerator.Reportings.ApplicationProfile;
using Coscine.KpiGenerator.Reportings.Project;
using Coscine.KpiGenerator.Reportings.Resource;
using Coscine.KpiGenerator.Reportings.System;
using Coscine.KpiGenerator.Reportings.User;
using Coscine.KpiGenerator.Utils;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using static KPIGenerator.Utils.CommandLineOptions;

namespace Coscine.KpiGenerator.Reportings.Complete;

public class CompleteReporting(
    ProjectReporting projectReporting,
    ResourceReporting resourceReporting,
    UserReporting userReporting,
    ApplicationProfileReporting applicationProfileReporting,
    SystemReporting systemReporting,
    ILogger<CompleteReporting> logger,
    [FromKeyedServices("gitlab")] IStorageService gitlabStorageService,
    [FromKeyedServices("local")] IStorageService localStorageService
    )
{
    private readonly ProjectReporting _projectReporting = projectReporting;
    private readonly ResourceReporting _resourceReporting = resourceReporting;
    private readonly UserReporting _userReporting = userReporting;
    private readonly ApplicationProfileReporting _applicationProfileReporting = applicationProfileReporting;
    private readonly SystemReporting _systemReporting = systemReporting;
    private readonly ILogger<CompleteReporting> _logger = logger;
    private readonly IStorageService _gitlabStorageService = gitlabStorageService;
    private readonly IStorageService _localStorageService = localStorageService;

    public CompleteReportingOptions Options { get; private set; } = null!;

    public async Task<bool> RunAsync(CompleteReportingOptions reportingOptions)
    {
        _logger.LogInformation("Generating Complete Reporting...");
        Options = reportingOptions;
        var reportingFiles = await GenerateReportingAsync();

        bool success = false;
        _logger.LogInformation("Publishing to GitLab...");
        // Publish to GitLab first, if that fails, publish to local storage
        try
        {
            if (!Options.DummyMode)
            {
                success = await _gitlabStorageService.PublishAsync("Complete Reporting", reportingFiles);
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "GitLab publish failed. Falling back to local storage.");
        }

        if (!success || Options.DummyMode)
        {
            _logger.LogInformation("Publishing to local storage instead.");
            success = await _localStorageService.PublishAsync("Complete Reporting", reportingFiles);
        }

        return success;
    }

    public async Task<IEnumerable<ReportingFileObject>> GenerateReportingAsync()
    {
        var reportingFiles = new List<ReportingFileObject>();

        var projectReportingFiles = await _projectReporting.GenerateReportingAsync();
        _logger.LogInformation("Project Reporting generated successfully.\n\n");
        var resourceReportingFiles = await _resourceReporting.GenerateReportingAsync();
        _logger.LogInformation("Resource Reporting generated successfully.\n\n");
        var userReportingFiles = await _userReporting.GenerateReportingAsync();
        _logger.LogInformation("User Reporting generated successfully.\n\n");
        var applicationProfileReportingFiles = await _applicationProfileReporting.GenerateReportingAsync();
        _logger.LogInformation("Application Profile Reporting generated successfully.\n\n");
        var systemReportingFiles = await _systemReporting.GenerateReportingAsync();
        _logger.LogInformation("System Reporting generated successfully.\n\n");

        reportingFiles.AddRange(projectReportingFiles);
        reportingFiles.AddRange(resourceReportingFiles);
        reportingFiles.AddRange(userReportingFiles);
        reportingFiles.AddRange(applicationProfileReportingFiles);
        reportingFiles.AddRange(systemReportingFiles);

        return reportingFiles;
    }
}
