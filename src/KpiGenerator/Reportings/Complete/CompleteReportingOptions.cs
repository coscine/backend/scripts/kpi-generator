﻿using CommandLine;

namespace KPIGenerator.Utils;

public static partial class CommandLineOptions
{
    [Verb("all", HelpText = "Generate all KPIs")]
    public class CompleteReportingOptions : BaseOptions
    {
        // Add verb specific options here
    }
}