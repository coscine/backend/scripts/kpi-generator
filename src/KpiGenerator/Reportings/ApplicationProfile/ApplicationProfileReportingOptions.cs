﻿using CommandLine;

namespace KPIGenerator.Utils;

public static partial class CommandLineOptions
{
    [Verb("applicationprofiles", HelpText = "Generate application profile KPIs")]
    public class ApplicationProfileReportingOptions : BaseOptions
    {
        // Add verb specific options here
    }
}