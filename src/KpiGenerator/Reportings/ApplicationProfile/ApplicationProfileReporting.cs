﻿using AutoMapper;
using Coscine.ApiClient;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Model;
using Coscine.KpiGenerator.Models;
using Coscine.KpiGenerator.Models.ConfigurationModels;
using Coscine.KpiGenerator.Utils;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using VDS.RDF;
using VDS.RDF.Nodes;
using VDS.RDF.Parsing;
using static KPIGenerator.Utils.CommandLineOptions;

namespace Coscine.KpiGenerator.Reportings.ApplicationProfile;

public class ApplicationProfileReporting
{
    private readonly IMapper _mapper;
    private readonly ILogger<ApplicationProfileReporting> _logger;
    private readonly IStorageService _gitlabStorageService;
    private readonly IStorageService _localStorageService;
    private readonly KpiConfiguration _kpiConfiguration;
    private readonly ReportingConfiguration _reportingConfiguration;
    private readonly IApplicationProfileApi _applicationProfileApi;

    public ApplicationProfileReportingOptions Options { get; private set; } = null!;
    public string ReportingFileName { get; }

    public ApplicationProfileReporting(
        IMapper mapper,
        ILogger<ApplicationProfileReporting> logger,
        [FromKeyedServices("gitlab")] IStorageService gitlabStorageService,
        [FromKeyedServices("local")] IStorageService localStorageService,
        IOptionsMonitor<KpiConfiguration> kpiConfiguration,
        IOptionsMonitor<ReportingConfiguration> reportingConfiguration,
        IApplicationProfileApi applicationProfileApi
    )
    {
        _mapper = mapper;
        _logger = logger;
        _gitlabStorageService = gitlabStorageService;
        _localStorageService = localStorageService;
        _kpiConfiguration = kpiConfiguration.CurrentValue;
        _reportingConfiguration = reportingConfiguration.CurrentValue;
        ReportingFileName = _kpiConfiguration.ApplicationProfileKpi.FileName;

        _applicationProfileApi = applicationProfileApi;
    }

    public async Task<bool> RunAsync(ApplicationProfileReportingOptions reportingOptions)
    {
        _logger.LogInformation("Generating Application Profile Reporting...");
        Options = reportingOptions;
        var reportingFiles = await GenerateReportingAsync();

        bool success = false;
        _logger.LogInformation("Publishing to GitLab...");
        // Publish to GitLab first, if that fails, publish to local storage
        try
        {
            if (!Options.DummyMode)
            {
                success = await _gitlabStorageService.PublishAsync("Application Profile Reporting", reportingFiles);
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "GitLab publish failed. Falling back to local storage.");
        }

        if (!success || Options.DummyMode)
        {
            _logger.LogInformation("Publishing to local storage instead.");
            success = await _localStorageService.PublishAsync("Application Profile Reporting", reportingFiles);
        }

        return success;
    }

    public async Task<IEnumerable<ReportingFileObject>> GenerateReportingAsync()
    {
        var reportingFiles = new List<ReportingFileObject>();
        var returnObjects = new List<ApplicationProfileReport>();

        _logger.LogDebug("Working on application profiles asynchronously...");
        var applicationProfiles = PaginationHelper.GetAllAsync<ApplicationProfileDtoPagedResponse, ApplicationProfileDto>(
            (pageNumber) => _applicationProfileApi.GetApplicationProfilesAsync(pageNumber: pageNumber, pageSize: 500));
        _logger.LogInformation("Relevant application profiles found.");

        await foreach (var ap in applicationProfiles)
        {
            _logger.LogDebug("Processing application profile {ap}.", ap.Uri);
            var g = new Graph();
            try
            {
                var applicationProfile = await _applicationProfileApi.GetApplicationProfileAsync(ap.Uri, RdfFormat.TextTurtle);
                _logger.LogDebug("Application profile retrieved. Parsing...");
                StringParser.Parse(g, applicationProfile.Data.Definition.Content);
                _logger.LogDebug("Application profile parsed.");
            }
            catch (Exception e)
            {
                _logger.LogWarning(e, "Failed to parse application profile {ap}. Skipping...", ap.Uri);
                continue;
            }

            var titleUri = new Uri("http://purl.org/dc/terms/title");
            var titles = g.GetTriplesWithPredicate(g.CreateUriNode(titleUri))
                            .Select(triple => triple.Object.AsValuedNode().AsString())
                            .ToList();
            _logger.LogDebug("Processed {n} titles.", titles.Count);

            var publisherUri = new Uri("http://purl.org/dc/terms/publisher");
            var publisher = g.GetTriplesWithPredicate(g.CreateUriNode(publisherUri))
                            .FirstOrDefault()?.Object.AsValuedNode().AsString();
            _logger.LogDebug("Processed publisher.");

            var rightsUri = new Uri("http://purl.org/dc/terms/rights");
            var rights = g.GetTriplesWithPredicate(g.CreateUriNode(rightsUri))
                            .FirstOrDefault()?.Object.AsValuedNode().AsString();
            _logger.LogDebug("Processed rights.");

            var licenseUri = new Uri("http://purl.org/dc/terms/license");
            var license = g.GetTriplesWithPredicate(g.CreateUriNode(licenseUri))
                            .FirstOrDefault()?.Object.AsValuedNode().AsString();
            _logger.LogDebug("Processed license.");

            returnObjects.Add(new ApplicationProfileReport
            {
                Uri = ap.Uri,
                Titles = titles ?? [],
                Publisher = publisher,
                Rights = rights,
                License = license
            });
        }
        _logger.LogInformation("{n} application profiles processed.", returnObjects.Count);

        // General File
        reportingFiles.Add(new ReportingFileObject
        {
            Path = Helpers.GetReportingPathGeneral(ReportingFileName),
            Content = Helpers.ConvertStringContentsToStream(JsonConvert.SerializeObject(returnObjects, Formatting.Indented))
        });
        _logger.LogInformation("General file generated.");

        // No per organization file

        return reportingFiles;
    }
}