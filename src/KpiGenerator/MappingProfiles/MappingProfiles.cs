﻿using AutoMapper;
using Coscine.ApiClient.Core.Model;
using Coscine.KpiGenerator.Models;

namespace Coscine.KpiGenerator.MappingProfiles;

public class MappingProfiles : Profile
{
    public MappingProfiles()
    {
        CreateMap<ProjectAdminDto, ProjectReport>()
            .ForMember(pr => pr.Id, opt => opt.MapFrom(dto => dto.Id))
            .ForMember(pr => pr.DateCreated, opt => opt.MapFrom(dto => dto.CreationDate))
            .ForMember(pr => pr.PublicationRequests, opt => opt.Ignore())
            .ForMember(pr => pr.Organizations, opt => opt.MapFrom(dto => dto.Organizations))
            .ForMember(pr => pr.Disciplines, opt => opt.MapFrom(dto => dto.Disciplines))
            .ForMember(pr => pr.Deleted, opt => opt.MapFrom(dto => dto.Deleted))
            .ForMember(pr => pr.ProjectVisibility, opt => opt.MapFrom(dto => dto.Visibility))
            .ForMember(pr => pr.Users, opt => opt.MapFrom(dto => dto.ProjectRoles.Count))
            .ForMember(pr => pr.ResourceTypeQuota, opt => opt.MapFrom(dto => dto.ProjectQuota))
            .ForMember(pr => pr.PublicationRequests, opt => opt.MapFrom(dto => dto.PublicationRequests));

        CreateMap<ResourceAdminDto, ResourceReport>()
            .ForMember(rr => rr.Id, opt => opt.MapFrom(dto => dto.Id))
            .ForMember(rr => rr.ApplicationProfile, opt => opt.MapFrom(dto => dto.ApplicationProfile.Uri))
            .ForMember(rr => rr.Archived, opt => opt.MapFrom(dto => dto.Archived))
            .ForMember(rr => rr.DateCreated, opt => opt.MapFrom(dto => dto.DateCreated))
            .ForMember(rr => rr.Deleted, opt => opt.MapFrom(dto => dto.Deleted))
            .ForMember(rr => rr.Disciplines, opt => opt.MapFrom(dto => dto.Disciplines))
            .ForMember(rr => rr.License, opt => opt.MapFrom(dto => dto.License.DisplayName))
            .ForMember(rr => rr.MetadataVisibility, opt => opt.MapFrom(dto => dto.Visibility))
            .ForMember(rr => rr.Organizations, opt => opt.Ignore())
            .ForMember(rr => rr.RelatedProjectId, opt => opt.MapFrom(dto => dto.ProjectResources.Select(pr => pr.ProjectId).FirstOrDefault()))
            .ForMember(rr => rr.ResourceQuota, opt => opt.MapFrom(dto => dto.ResourceQuota))
            .ForMember(rr => rr.ResourceType, opt => opt.MapFrom(dto => dto.Type.SpecificType));

        CreateMap<UserDto, UserReport>()
            .ForMember(ur => ur.Disciplines, opt => opt.MapFrom(dto => dto.Disciplines))
            .ForMember(ur => ur.LatestActivity, opt => opt.MapFrom(dto => dto.LatestActivity))
            .ForMember(ur => ur.LoginProviders, opt => opt.MapFrom(dto => dto.Identities))
            .ForMember(ur => ur.Organizations, opt => opt.MapFrom(dto => dto.Organizations))
            .ForMember(ur => ur.RelatedProjects, opt => opt.Ignore());

        CreateMap<ProjectRoleDto, RelatedProject>()
            .ForMember(rp => rp.ProjectId, opt => opt.MapFrom(dto => dto.Project.Id))
            .ForMember(rp => rp.Role, opt => opt.MapFrom(dto => dto.Role.DisplayName));

        CreateMap<IdentityProviderDto, LoginProvider>()
            .ForMember(lp => lp.Id, opt => opt.MapFrom(dto => dto.Id))
            .ForMember(lp => lp.DisplayName, opt => opt.MapFrom(dto => dto.DisplayName));

        CreateMap<UserOrganizationDto, UserOrganization>()
            .ForMember(o => o.ReadOnly, opt => opt.MapFrom(dto => dto.ReadOnly))
            .ForMember(o => o.Name, opt => opt.MapFrom(dto => dto.DisplayName))
            .ForMember(o => o.RorUrl, opt => opt.MapFrom(dto => dto.Uri));

        CreateMap<OrganizationDto, Organization>()
            .ForMember(o => o.Name, opt => opt.MapFrom(dto => dto.DisplayName))
            .ForMember(o => o.RorUrl, opt => opt.MapFrom(dto => dto.Uri));
        
        CreateMap<ProjectOrganizationDto, ProjectOrganization>()
            .ForMember(po => po.Name, opt => opt.MapFrom(dto => dto.DisplayName))
            .ForMember(po => po.RorUrl, opt => opt.MapFrom(dto => dto.Uri))
            .ForMember(po => po.Responsible, opt => opt.MapFrom(dto => dto.Responsible));

        CreateMap<VisibilityDto, ProjectVisibility>()
            .ForMember(pv => pv.Id, opt => opt.MapFrom(dto => dto.Id))
            .ForMember(pv => pv.DisplayName, opt => opt.MapFrom(dto => dto.DisplayName));

        CreateMap<VisibilityDto, MetadataVisibility>()
            .ForMember(pv => pv.Id, opt => opt.MapFrom(dto => dto.Id))
            .ForMember(pv => pv.DisplayName, opt => opt.MapFrom(dto => dto.DisplayName));

        CreateMap<DisciplineDto, Discipline>()
            .ForMember(d => d.Id, opt => opt.MapFrom(dto => dto.Id))
            .ForMember(d => d.Url, opt => opt.MapFrom(dto => dto.Uri))
            .ForMember(d => d.DisplayNameDe, opt => opt.MapFrom(dto => dto.DisplayNameDe))
            .ForMember(d => d.DisplayNameEn, opt => opt.MapFrom(dto => dto.DisplayNameEn));

        CreateMap<ResourceQuotaDto, ResourceQuota>()
            .ForMember(rq => rq.Reserved, opt => opt.MapFrom(dto => dto.Reserved))
            .ForMember(rq => rq.Used, opt => opt.MapFrom(dto => dto.Used))
            .ForMember(rq => rq.UsedPercentage, opt => opt.MapFrom(dto => dto.UsedPercentage));

        CreateMap<QuotaDto, Quota>()
            .ForMember(q => q.Value, opt => opt.MapFrom(dto => dto.Value))
            .ForMember(q => q.Unit, opt => opt.MapFrom(dto => dto.Unit));

        CreateMap<ProjectQuotaDto, ResourceTypeQuota>()
            .ForMember(rtq => rtq.ResourceType, opt => opt.MapFrom(dto => dto.ResourceType.SpecificType))
            .ForMember(rtq => rtq.TotalUsed, opt => opt.MapFrom(dto => dto.TotalUsed))
            .ForMember(rtq => rtq.TotalReserved, opt => opt.MapFrom(dto => dto.TotalReserved))
            .ForMember(rtq => rtq.Allocated, opt => opt.MapFrom(dto => dto.Allocated))
            .ForMember(rtq => rtq.Maximum, opt => opt.MapFrom(dto => dto.Maximum));

        CreateMap<ProjectPublicationRequestDto, PublicationRequestReport>()
            .ForMember(prr => prr.PublicationServiceRorId, opt => opt.MapFrom(dto => dto.PublicationServiceRorId))
            .ForMember(prr => prr.DateCreated, opt => opt.MapFrom(dto => dto.DateCreated))
            .ForMember(prr => prr.Resources, opt => opt.MapFrom(dto => dto.Resources));
        }
}
