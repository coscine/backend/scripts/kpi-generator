﻿using System.Text.Json.Serialization;

namespace Coscine.KpiGenerator.Models;

public record MaintenanceReport
{
    [JsonPropertyName("id")]
    public int Id { get; init; }
    [JsonPropertyName("title")]
    public string Title { get; init; } = null!;
    [JsonPropertyName("short_message")]
    public string ShortMessage { get; init; } = null!;
    [JsonPropertyName("message")]
    public string Message { get; init; } = null!;
    [JsonPropertyName("status")]
    public string Status { get; init; } = null!;
    [JsonPropertyName("start_at")]
    public DateTime? StartAt { get; init; } = null!;
    [JsonPropertyName("end_at")]
    public DateTime? EndAt { get; init; } = null!;
}

