﻿using System.Text.Json.Serialization;

namespace Coscine.KpiGenerator.Models;

public record ResourceReport
{
    [JsonPropertyName("Id")]
    public string Id { get; init; } = null!;

    [JsonPropertyName("ResourceType")]
    public string ResourceType { get; init; } = null!;

    [JsonPropertyName("DateCreated")]
    public DateTime? DateCreated { get; init; } = null!;

    [JsonPropertyName("Archived")]
    public bool Archived { get; init; }

    [JsonPropertyName("Deleted")]
    public bool Deleted { get; init; }

    [JsonPropertyName("MetadataVisibility")]
    public MetadataVisibility MetadataVisibility { get; init; } = null!;

    [JsonPropertyName("RelatedProjectId")]
    public string RelatedProjectId { get; init; } = null!;

    [JsonPropertyName("Organizations")]
    public IReadOnlyList<ProjectOrganization> Organizations { get; set; } = [];

    [JsonPropertyName("Disciplines")]
    public IReadOnlyList<Discipline> Disciplines { get; init; } = null!;

    [JsonPropertyName("License")]
    public string License { get; init; } = null!;

    [JsonPropertyName("ApplicationProfile")]
    public string ApplicationProfile { get; init; } = null!;

    [JsonPropertyName("ResourceQuota")]
    public ResourceQuota ResourceQuota { get; set; } = null!;
}