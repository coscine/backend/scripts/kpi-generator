﻿using System.Text.Json.Serialization;

namespace Coscine.KpiGenerator.Models;

public record Discipline
{
    [JsonPropertyName("Id")]
    public string Id { get; init; } = null!;

    [JsonPropertyName("Url")]
    public string Url { get; init; } = null!;

    [JsonPropertyName("DisplayNameDe")]
    public string DisplayNameDe { get; init; } = null!;

    [JsonPropertyName("DisplayNameEn")]
    public string DisplayNameEn { get; init; } = null!;
}
