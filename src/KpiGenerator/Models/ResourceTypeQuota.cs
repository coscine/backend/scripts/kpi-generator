﻿using System.Text.Json.Serialization;

namespace Coscine.KpiGenerator.Models;

public record ResourceTypeQuota
{
    [JsonPropertyName("ResourceType")]
    public string ResourceType { get; init; } = null!;

    [JsonPropertyName("TotalUsed")]
    public Quota TotalUsed { get; init; } = null!;

    [JsonPropertyName("TotalReserved")]
    public Quota TotalReserved { get; init; } = null!;

    [JsonPropertyName("Allocated")]
    public Quota Allocated { get; init; } = null!;

    [JsonPropertyName("Maximum")]
    public Quota Maximum { get; init; } = null!;
}
