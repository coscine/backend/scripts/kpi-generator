﻿using System.Text.Json.Serialization;

namespace Coscine.KpiGenerator.Models;

public record MetadataVisibility
{
    [JsonPropertyName("Id")]
    public string Id { get; init; } = null!;

    [JsonPropertyName("DisplayName")]
    public string DisplayName { get; init; } = null!;
}
