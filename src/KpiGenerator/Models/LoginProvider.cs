﻿using System.Text.Json.Serialization;

namespace Coscine.KpiGenerator.Models;

public record LoginProvider
{
    [JsonPropertyName("Id")]
    public string Id { get; init; } = null!;

    [JsonPropertyName("DisplayName")]
    public string DisplayName { get; init; } = null!;
}
