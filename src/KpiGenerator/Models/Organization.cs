﻿using System.Text.Json.Serialization;

namespace Coscine.KpiGenerator.Models;

public record Organization
{
    /// <summary>
    /// Organizaiton name from GitLab project's description
    /// </summary>
    /// <example>RWTH Aachen University</example>
    [JsonPropertyName("Name")]
    public string Name { get; init; } = null!;

    /// <summary>
    /// Organizaiton ROR URL from GitLab project's title
    /// </summary>
    /// <example>https://ror.org/04xfq0f34</example>
    [JsonPropertyName("RorUrl")]
    public string RorUrl { get; init; } = null!;
}