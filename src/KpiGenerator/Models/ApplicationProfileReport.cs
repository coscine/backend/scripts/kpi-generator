﻿namespace Coscine.KpiGenerator.Models;

/// <summary>
/// Object containing the JSON structure for the reporting
/// </summary>
public class ApplicationProfileReport
{
    public IReadOnlyList<string> Titles { get; set; } = [];
    public string Uri { get; set; } = null!;
    public string? Publisher { get; set; } = null!;
    public string? Rights { get; set; } = null!;
    public string? License { get; set; } = null!;
}
