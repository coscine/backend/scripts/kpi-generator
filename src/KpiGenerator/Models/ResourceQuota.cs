﻿using System.Text.Json.Serialization;

namespace Coscine.KpiGenerator.Models;

public record ResourceQuota
{
    [JsonPropertyName("Used")]
    public Quota Used { get; init; } = null!;

    [JsonPropertyName("UsedPercentage")]
    public double UsedPercentage { get; init; }

    [JsonPropertyName("Reserved")]
    public Quota Reserved { get; init; } = null!;
}
