using System.Text.Json.Serialization;

namespace Coscine.KpiGenerator.Models;

public record UserOrganization : Organization {
    /// <summary>
    /// Determines if the organization's details can be modified.
    /// </summary>
    /// <value><c>true</c> if the organization is read-only; otherwise, <c>false</c>.</value>
    /// <remarks>
    /// This property defaults to <c>true</c> and can be manually set to <c>false</c>.
    /// For entries set by Shibboleth, this property is automatically set to <c>true</c>.
    /// </remarks>
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("ReadOnly")]
    public bool? ReadOnly { get; init; }
}