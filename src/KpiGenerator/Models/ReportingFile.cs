﻿namespace Coscine.KpiGenerator.Models;

public class ReportingFileObject
{
    /// <summary>
    /// Relative file path in the GitLab project tree containing the file name and extension.
    /// </summary>
    /// <example>General/users.json</example>
    public string Path { get; set; } = null!;
    /// <summary>
    /// File contents as a Stream
    /// </summary>
    public Stream Content { get; set; } = null!;
}
