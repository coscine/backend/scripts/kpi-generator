﻿using System.Text.Json.Serialization;

namespace Coscine.KpiGenerator.Models;

public record Quota
{
    [JsonPropertyName("Value")]
    public double Value { get; init; }

    [JsonPropertyName("Unit")]
    public string Unit { get; init; } = null!;
}
