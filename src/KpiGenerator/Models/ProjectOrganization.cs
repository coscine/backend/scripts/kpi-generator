﻿namespace Coscine.KpiGenerator.Models;

public record ProjectOrganization : Organization
{
    /// <summary>
    /// Determines if the organization is Responsible for a given project.
    /// </summary>
    public bool Responsible { get; set; }
}