﻿using System.Text.Json.Serialization;

namespace Coscine.KpiGenerator.Models;

public record ProjectReport
{
    [JsonPropertyName("Id")]
    public Guid Id { get; init; }

    [JsonPropertyName("DateCreated")]
    public DateTime? DateCreated { get; init; } = null!;

    [JsonPropertyName("PublicationRequests")]
    public IReadOnlyList<PublicationRequestReport> PublicationRequests { get; set; } = null!;

    [JsonPropertyName("Organizations")]
    public IReadOnlyList<ProjectOrganization> Organizations { get; init; } = null!;

    [JsonPropertyName("Disciplines")]
    public IReadOnlyList<Discipline> Disciplines { get; init; } = null!;

    [JsonPropertyName("Deleted")]
    public bool Deleted { get; init; }

    [JsonPropertyName("ProjectVisibility")]
    public ProjectVisibility ProjectVisibility { get; init; } = null!;

    [JsonPropertyName("Users")]
    public int Users { get; init; }

    [JsonPropertyName("ResourceTypeQuota")]
    public IReadOnlyList<ResourceTypeQuota> ResourceTypeQuota { get; init; } = null!;
}