﻿using System.Text.Json.Serialization;

namespace Coscine.KpiGenerator.Models;

public record RelatedProject
{
    [JsonPropertyName("ProjectId")]
    public string ProjectId { get; init; } = null!;

    [JsonPropertyName("Role")]
    public string Role { get; init; } = null!;
}
