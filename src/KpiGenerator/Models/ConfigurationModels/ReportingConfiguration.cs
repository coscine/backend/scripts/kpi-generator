﻿namespace Coscine.KpiGenerator.Models.ConfigurationModels;

/// <summary>
/// Represents the configuration settings for the reporting.
/// </summary>
public class ReportingConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "ReportingConfiguration";

    /// <summary>
    /// Value indicating whether the reporting is enabled.
    /// </summary>
    public bool IsEnabled { get; init; }

    /// <summary>
    /// API token for the coscine API.
    /// Requires the administrator role.
    /// </summary>
    public string ApiKey { get; set; } = null!;

    /// <summary>
    /// Address of the Coscine API.
    /// </summary>
    public string Endpoint { get; set; } = null!;

    /// <summary>
    /// Local storage configuration settings.
    /// </summary>
    /// <value>
    /// The local storage configuration settings, or <c>null</c> if not configured.
    /// </value>
    public LocalStorageConfiguration? Local { get; init; }

    /// <summary>
    /// Logger configuration settings.
    /// </summary>
    /// <value>
    /// The logger storage configuration settings, or <c>null</c> if not configured.
    /// </value>
    public LoggerConfiguration? Logger { get; init; }

    /// <summary>
    /// GitLab configuration settings.
    /// </summary>
    /// <value>
    /// The GitLab configuration settings, or <c>null</c> if not configured.
    /// </value>
    public GitLabConfiguration? GitLab { get; init; }

    /// <summary>
    /// Organization configuration settings.
    /// </summary>
    /// <value>
    /// The organization configuration settings, or <c>null</c> if not configured.
    /// </value>
    public OrganizationConfiguration? Organization { get; init; }

    /// <summary>
    /// Files to keep inside the repository, when comitting.
    /// </summary>
    /// <value>
    /// The files must contain their path too, relative to the GitLab project root.
    /// </value>
    public IReadOnlyList<string> FilesToKeepInRepo { get; init; } = [];

    /// <summary>
    /// Represents the configuration settings for local storage.
    /// </summary>
    public record LocalStorageConfiguration(string? StoragePath);

    /// <summary>
    /// Represents the configuration settings for GitLab.
    /// </summary>
    public record GitLabConfiguration(string? HostUrl, string? Token, string? Branch, int? ProjectId);

    /// <summary>
    /// Represents the configuration settings for an organization.
    /// </summary>
    public record OrganizationConfiguration
    {
        /// <summary>
        /// Gets or sets the configuration for another organization.
        /// </summary>
        public Organization? OtherOrganization { get; init; }
    }

    /// <summary>
    /// Represents the configuration settings for the logger.
    /// </summary>
    public record LoggerConfiguration(string? LogLevel, string? LogHome);
}
