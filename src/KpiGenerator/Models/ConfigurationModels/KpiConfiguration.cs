﻿namespace Coscine.KpiGenerator.Models.ConfigurationModels;

/// <summary>
/// Represents the configuration settings for the KPIs used in the application.
/// </summary>
public class KpiConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "KpiConfiguration";

    /// <summary>
    /// Indicates whether the issuer should be validated during token validation.
    /// </summary>
    public ApplicationProfileKpi ApplicationProfileKpi { get; init; } = null!;

    /// <summary>
    /// Indicates whether the audience should be validated during token validation.
    /// </summary>
    public ProjectKpi ProjectKpi { get; init; } = null!;

    /// <summary>
    /// Indicates whether the lifetime should be validated during token validation.
    /// </summary>
    public ResourceKpi ResourceKpi { get; init; } = null!;

    /// <summary>
    /// Indicates whether the issuer signing key should be validated during token validation.
    /// </summary>
    public SystemKpi SystemKpi { get; init; } = null!;

    /// <summary>
    /// A list of valid issuers for the JWT.
    /// </summary>
    public UserKpi UserKpi { get; init; } = null!;
}

public record ApplicationProfileKpi(string FileName);

public record Maintenance
{
    public string Url { get; init; } = null!;
    public string Username { get; init; } = null!;
    public string Password { get; init; } = null!;
    public string BasicAuthToken { get; init; } = null!;
}

public record ProjectKpi(string FileName);

public record ResourceKpi(string FileName);

public record SystemKpi
{
    public string FileName { get; init; } = null!;
    public Maintenance Maintenance { get; init; } = null!;
}

public record UserKpi(string FileName);
