﻿using System.Text.Json.Serialization;
using Coscine.ApiClient.Core.Model;

namespace Coscine.KpiGenerator.Models;

public record PublicationRequestReport
{
    [JsonPropertyName("PublicationServiceRorId")]
    public Uri PublicationServiceRorId { get; init; } = null!;

    [JsonPropertyName("DateCreated")]
    public DateTime DateCreated { get; set; }

    [JsonPropertyName("Resources")]
    public IReadOnlyList<ResourceMinimalDto> Resources { get; set; } = null!;
}