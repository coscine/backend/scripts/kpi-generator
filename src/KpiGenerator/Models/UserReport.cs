﻿namespace Coscine.KpiGenerator.Models;

public record UserReport
{
    public IReadOnlyList<RelatedProject> RelatedProjects { get; set; } = null!;

    public IReadOnlyList<UserOrganization> Organizations { get; init; } = null!;

    public IReadOnlyList<Discipline> Disciplines { get; init; } = null!;

    public IReadOnlyList<LoginProvider> LoginProviders { get; init; } = null!;

    public DateTime? LatestActivity { get; set; } = null!;
}
