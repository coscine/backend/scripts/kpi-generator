﻿namespace Coscine.KpiGenerator.Models;

/// <summary>
/// Object containing the JSON structure for the reporting
/// </summary>
public record SystemReport
{
    public IReadOnlyList<MaintenanceReport>? Banners { get; init; } = new List<MaintenanceReport>();
}