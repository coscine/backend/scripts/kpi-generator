﻿using CommandLine;

namespace KPIGenerator.Utils;

public static partial class CommandLineOptions
{
    public class BaseOptions
    {
        [Option("dummy", Required = false, Default = false, HelpText = "An argument that tells the program to execute in dummy mode.")]
        public bool DummyMode { get; set; }
    }
}