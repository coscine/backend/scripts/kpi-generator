﻿using Coscine.KpiGenerator.Models;

namespace Coscine.KpiGenerator.Utils;

public static class GitLabUtils
{
    //public static void EnsureGitLabInformationIsSetAndCorrect()
    //{
    //    if (!ReportingEnabled)
    //    {
    //        throw new ApplicationException($"\nReporting is deactivated on this machine! \nTo enable it, set the Consul Key \"coscine/local/reporting/enabled\" to  \"true\".");
    //    }
    //    if (string.IsNullOrWhiteSpace(ReportingDatabaseProjectId) || string.IsNullOrWhiteSpace(ReportingBranch))
    //    {
    //        throw new ArgumentNullException($"\nNo valid Reporting Project ID or Branch were provided!");
    //    }
    //    var project = GitLabClient.Projects.GetAsync(ReportingDatabaseProjectId).Result;
    //    _logger.LogInformation($" - Report Generation to be uploaded to GitLab Project \"{project.Name}\" on branch \"{ReportingBranch}\"");
    //    _logger.LogInformation("Report Generation to be uploaded to GitLab Project {projectName} on branch {ReportingBranch}", project.Name, ReportingBranch);
    //    var branch = GitLabClient.Branches.GetAsync(project.Id, o => o.Search = ReportingBranch).Result;

    //    if (!branch.Any(b => b.Name.Equals(ReportingBranch)) && Domain.Equals("DEVLEF") && !project.DefaultBranch.Equals(ReportingBranch))
    //    {
    //        _logger.LogInformation($" - Branch \"{ReportingBranch}\" does not exist. Working on Domain {Domain}. Creating branch...");
    //        GitLabClient.Branches.CreateAsync(ReportingDatabaseProjectId, new CreateBranchRequest(ReportingBranch, project.DefaultBranch)).Wait();
    //        _logger.LogInformation($" - Branch \"{ReportingBranch}\" successfully created");
    //        _logger.LogInformation("Branch {ReportingBranch} successfully created", ReportingBranch);
    //    }
    //    else if (!branch.Any(b => b.Name.Equals(ReportingBranch)))
    //    {
    //        throw new ArgumentNullException($"\nBranch \"{ReportingBranch}\" does not exist!");
    //    }
    //    _logger.LogInformation();
    //}

    public static Task<bool> PublishAsync(IEnumerable<ReportingFileObject> reportingFiles)
    {
        throw new NotImplementedException();
    }
}