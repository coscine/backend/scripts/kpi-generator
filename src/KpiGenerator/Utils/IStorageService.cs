﻿using Coscine.KpiGenerator.Models;

namespace Coscine.KpiGenerator.Utils;

public interface IStorageService
{
    Task EnsureInformationIsSetAndCorrectAsync();
    Task<bool> PublishAsync(string reportingInstanceName, IEnumerable<ReportingFileObject> files);
}