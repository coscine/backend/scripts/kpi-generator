﻿using Coscine.KpiGenerator.Models;
using System.Text;
using System.Web;

namespace Coscine.KpiGenerator.Utils;

public static class Helpers
{
    public static IEnumerable<Organization> GetTopLevelOrganizationsFromEntries(IEnumerable<Organization?> organizations)
    {
        var result = new List<Organization>();
        foreach (var org in organizations.Where(o => o is not null && o.RorUrl is not null))
        {
            var ror = org?.RorUrl;
            if (string.IsNullOrWhiteSpace(ror) || org is null)
            {
                continue;
            }
            result.Add(new Organization
            {
                RorUrl = ror.Contains('#') ? ror[..ror.IndexOf('#')] : ror, // e.g. <https://ror.org/04xfq0f34#ORG-42NHW> turns into <https://ror.org/04xfq0f34>
                Name = org.Name
            });
        }
        return result.DistinctBy(r => r.RorUrl);
    }

    public static string GetReportingPathGeneral(string fileName) => string.Format("General/{0}", fileName);

    public static string GetReportingPathOrganization(string organizationRor, string fileName) => string.Format("Organizations/{0}/{1}", SanitizeOrganizationRor(organizationRor), fileName);

    public static string SanitizeOrganizationRor(string organizationRor) => HttpUtility.UrlEncode(organizationRor.Replace("https://ror.org/", "").ToLower());

    public static Stream ConvertStringContentsToStream(string contents) => new MemoryStream(Encoding.UTF8.GetBytes(contents));
}
