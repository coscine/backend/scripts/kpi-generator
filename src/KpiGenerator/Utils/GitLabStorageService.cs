﻿using Coscine.KpiGenerator.Models;
using Coscine.KpiGenerator.Models.ConfigurationModels;
using GitLabApiClient;
using GitLabApiClient.Models.Branches.Requests;
using GitLabApiClient.Models.Commits.Requests.CreateCommitRequest;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Coscine.KpiGenerator.Utils;

public class GitLabStorageService : IStorageService
{
    private readonly GitLabClient _gitLabClient;
    private readonly ReportingConfiguration _reportingConfiguration;
    private readonly ILogger<GitLabStorageService> _logger;

    public GitLabStorageService(
        ILogger<GitLabStorageService> logger,
        IOptionsMonitor<ReportingConfiguration> reportingConfiguration
    )
    {
        _reportingConfiguration = reportingConfiguration.CurrentValue;
        _gitLabClient = new GitLabClient(_reportingConfiguration.GitLab?.HostUrl, _reportingConfiguration.GitLab?.Token);
        _logger = logger;
    }

    public async Task EnsureInformationIsSetAndCorrectAsync()
    {
        if (!_reportingConfiguration.IsEnabled)
        {
            throw new ApplicationException($"\nReporting is deactivated on this machine! \nTo enable it, set the key \"ReportingConfiguration:IsEnabled\" under \"coscine/Coscine.Infrastructure/KpiGenerator/appsettings\" to \"true\".");
        }
        if (_reportingConfiguration.GitLab?.ProjectId is null || string.IsNullOrWhiteSpace(_reportingConfiguration.GitLab?.Branch))
        {
            throw new ArgumentNullException($"\nNo valid Reporting Project ID or Branch were provided! Ensure the configuration under \"coscine/Coscine.Infrastructure/KpiGenerator/appsettings\" is correct.");
        }
        var project = await _gitLabClient.Projects.GetAsync(_reportingConfiguration.GitLab?.ProjectId);
        var branch = await _gitLabClient.Branches.GetAsync(project.Id, o => o.Search = _reportingConfiguration.GitLab?.Branch);

        if (!branch.Any(b => b.Name.Equals(_reportingConfiguration.GitLab?.Branch)) && !project.DefaultBranch.Equals(_reportingConfiguration.GitLab?.Branch))
        {
            await _gitLabClient.Branches.CreateAsync(_reportingConfiguration.GitLab?.ProjectId, new CreateBranchRequest(_reportingConfiguration.GitLab?.Branch, project.DefaultBranch));
        }
        else if (!branch.Any(b => b.Name.Equals(_reportingConfiguration.GitLab?.Branch)))
        {
            throw new ArgumentNullException($"\nBranch \"{_reportingConfiguration.GitLab?.Branch}\" does not exist!");
        }
    }

    public async Task<bool> PublishAsync(string reportingInstanceName, IEnumerable<ReportingFileObject> files)
    {
        await EnsureInformationIsSetAndCorrectAsync();
        try
        {
            // Retrieve Reporting Database project
            var reportingDatabaseProject = await _gitLabClient.Projects.GetAsync(_reportingConfiguration.GitLab?.ProjectId);
            var commitBranch = _reportingConfiguration.GitLab?.Branch;
            var commitMessage = $"{reportingInstanceName} Generated - {DateTime.Now:dd.MM.yyyy HH:mm}"; // CompleteReporting Generated - 31.08.2022 10:25

            var projectTree = await _gitLabClient.Trees.GetAsync(reportingDatabaseProject, o =>
            {
                o.Recursive = true;
                o.Reference = commitBranch;
            });

            // Define commit actions
            var actions = new List<CreateCommitRequestAction>();

            // Delete files or organizations that are not valid anymore
            foreach (var fileInProject in projectTree.Where(file => file.Type.Equals("blob")))
            {
                // Delete files, that are not part of "files" and are not any of the files to keep
                if (!files.Any(f => f.Path.Equals(fileInProject.Path)) && !_reportingConfiguration.FilesToKeepInRepo.Any(f => f.Equals(fileInProject.Path)))
                {
                    // Add Action
                    actions.Add(new CreateCommitRequestAction(CreateCommitRequestActionType.Delete, fileInProject.Path));
                }
            }

            // Create a commit per file with its contents
            foreach (var file in files)
            {
                // Write file contents to bytes
                byte[] bytes;
                using (var ms = new MemoryStream())
                {
                    file.Content.CopyTo(ms);
                    bytes = ms.ToArray();
                }

                // Distinguish between Creating or Updating a file
                var actionType = CreateCommitRequestActionType.Create;
                if (projectTree.Any(f => f.Path.Equals(file.Path)))
                {
                    actionType = CreateCommitRequestActionType.Update;
                }

                // Add Action
                actions.Add(new CreateCommitRequestAction(actionType, file.Path)
                {
                    Content = Convert.ToBase64String(bytes),
                    Encoding = CreateCommitRequestActionEncoding.Base64
                });
            }
            // Push Commit
            await _gitLabClient.Commits.CreateAsync(reportingDatabaseProject, new CreateCommitRequest(commitBranch, commitMessage, actions));
            return true;
        }
        catch (Exception e)
        {
            _logger.LogInformation("Exception: {e}", e.Message);
            return false;
        }
    }
}
