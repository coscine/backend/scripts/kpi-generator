﻿using Coscine.KpiGenerator.Models;
using Coscine.KpiGenerator.Models.ConfigurationModels;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Coscine.KpiGenerator.Utils;

public class FileSystemStorageService(
    ILogger<FileSystemStorageService> logger,
    IOptionsMonitor<ReportingConfiguration> reportingConfiguration) : IStorageService
{
    private readonly ReportingConfiguration _reportingConfiguration = reportingConfiguration.CurrentValue;
    private readonly ILogger<FileSystemStorageService> _logger = logger;
    private string? _localStoragePath;

    public Task EnsureInformationIsSetAndCorrectAsync()
    {
        // Ensure that the base file path from the configuration is valid
        if (string.IsNullOrWhiteSpace(_reportingConfiguration.Local?.StoragePath))
        {
            throw new ApplicationException("No valid file storage path provided in the reporting configuration.");
        }

        // Create the base directory if it doesn't exist
        if (!Directory.Exists(_reportingConfiguration.Local.StoragePath))
        {
            Directory.CreateDirectory(_reportingConfiguration.Local.StoragePath);
        }

        // Create a subdirectory for the current date
        _localStoragePath = Path.Combine(_reportingConfiguration.Local.StoragePath, DateTime.Now.ToString("yyyy-MM-dd"));
        if (!Directory.Exists(_localStoragePath))
        {
            Directory.CreateDirectory(_localStoragePath);
        }

        return Task.CompletedTask;
    }

    public async Task<bool> PublishAsync(string reportingInstanceName, IEnumerable<ReportingFileObject> files)
    {
        await EnsureInformationIsSetAndCorrectAsync();
        try
        {
            foreach (var file in files)
            {
                var filePath = Path.Combine(_localStoragePath ?? $"C:/coscine/reporting/temp/{reportingInstanceName}/", file.Path);
                var directoryPath = Path.GetDirectoryName(filePath);

                // Ensure the directory exists
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                // Write file contents to disk
                using var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write);

                // Reset the stream position to the beginning
                if (file.Content.CanSeek)
                {
                    file.Content.Seek(0, SeekOrigin.Begin);
                }
                await file.Content.CopyToAsync(fileStream);
                await fileStream.FlushAsync(); // Ensure all data is written to disk
            }
            return true;
        }
        catch (Exception e)
        {
            _logger.LogInformation("Error saving files to file system: {e}", e.Message);
            return false;
        }
    }
}