FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build-env
WORKDIR /App

# Copy everything
COPY . ./

# Add nuget sources for private packages (here: api-client)
RUN dotnet nuget add source "https://git.rwth-aachen.de/api/v4/projects/88930/packages/nuget/index.json" -n "api-client"

# Restore as distinct layers
RUN dotnet restore

# Build and publish a release
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:8.0

# Install cron and other necessary packages
RUN apt-get update && \
    apt-get install -y cron && \
    apt-get clean

WORKDIR /App
COPY --from=build-env /App/out .

# Set the build-time argument and default environment variable
ENV DOTNET_ENVIRONMENT=Development

# Create the log file
RUN touch /var/log/cron.log

ENTRYPOINT /bin/sh -c "/App/Coscine.KpiGenerator all"