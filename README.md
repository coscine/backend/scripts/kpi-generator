# KPI Generator

[[_TOC_]]

## 📝 Overview

The KPI Generator collects information from Virtuoso and the SQL Database by connecting to our databases. The program then writes, saves and uploads JSON reporting files to a specific GitLab project in a specific structure.

GitLab structure:  
- :fox: [`{Reporting Project}`](https://git.rwth-aachen.de/coscine/reporting/reporting-database)
  - :open_file_folder:  `General`
      -  :notepad_spiral: `users.json` (File containing User related KPIs extracted from Coscine #2182)
      - :notepad_spiral: `projects.json` (File containing Project related KPIs extracted from Coscine #2184)
      - :notepad_spiral: `resources.json` (File containing Resource related KPIs extracted from Coscine #2183)
      - :notepad_spiral: `application_profiles.json` (File containing Application Profile related KPIs extracted from Coscine #2185)
      - :notepad_spiral: `system_status.json` (File containing System Status related KPIs extracted from Coscine #2186)
  - :open_file_folder:  `Organizations`
    - :open_file_folder:  `{Organization ROR ID}` (Folder per organization)
      - :notepad_spiral: `users.json` (File containing User related KPIs extracted from Coscine #2182)
      - :notepad_spiral: `projects.json` (File containing Project related KPIs extracted from Coscine #2184)
      - :notepad_spiral: `resources.json` (File containing Resource related KPIs extracted from Coscine #2183)
    - :file_folder: `...`
      - :notepad_spiral: `...` 
  - :notepad_spiral: `README.md` 

This is a C# program that takes command line arguments and generates various reports based on the options passed. It uses the CommandLine library to parse the arguments and then maps the options to corresponding reporting classes. The program also logs events using the NLog library, and exceptions are caught and logged as warnings with their corresponding messages. The SanitizeOptions function is used to sanitize input options based on their types.

## ⚙️ Configuration

Before you can run and use the script, you need to ensure that the following dependencies and prerequisites are in place:

1. The project's referenced .NET SDK(s) must be installed. Please refer to the project's source code for information on which .NET SDK(s) are required.
2. Use the `appsettings.json` as a tempate or a base for your configuration of the script. Fill it out accordingly.

Once you have all the necessary dependencies and prerequisites in place, you should be able to run and use this script.

## 📖 Usage

To get started with this project, you will need to ensure you have configured and built it correctly. 

1. Execute the built executable (`.exe`)

### Generating reporting files

- **Executing in dummy mode**
  ```powershell
  .\Coscine.KpiGenerator.exe <VERB> --dummy
  ```

- **Complete Reporting** (all)
  ```powershell
  .\Coscine.KpiGenerator.exe all
  ```
- **Only Project Reporting**
  ```powershell
  .\Coscine.KpiGenerator.exe projects
  ```
- **Only Resource Reporting**
  ```powershell
  .\Coscine.KpiGenerator.exe resources
  ```
- **Only User Reporting**
  ```powershell
  .\Coscine.KpiGenerator.exe users
  ```
- **Only Application Profile Reporting**
  ```powershell
  .\Coscine.KpiGenerator.exe applprofiles
  ```
- **Only System Status Reporting**
  ```powershell
  .\Coscine.KpiGenerator.exe system
  ```

### Integrate to Coscine's infrastructure as a CRON Job

Running the [Integrator Script](https://git.rwth-aachen.de/coscine/tools/integrator-script) will add the script as a CRON Job, that will automatically trigger at 03:00 am every day.

## 👥 Contributing

As an open source plattform and project, we welcome contributions from our community in any form. You can do so by submitting bug reports or feature requests, or by directly contributing to Coscine's source code. To submit your contribution please follow our [Contributing Guideline](https://git.rwth-aachen.de/coscine/docs/public/wiki/-/blob/master/Contributing%20To%20Coscine.md).

## 📄 License

The current open source repository is licensed under the **MIT License**, which is a permissive license that allows the software to be used, modified, and distributed for both commercial and non-commercial purposes, with limited restrictions (see `LICENSE` file)

> The MIT License allows for free use, modification, and distribution of the software and its associated documentation, subject to certain conditions. The license requires that the copyright notice and permission notice be included in all copies or substantial portions of the software. The software is provided "as is" without any warranties, and the authors or copyright holders cannot be held liable for any damages or other liability arising from its use.

## 🆘 Support

1. **Check the documentation**: Before reaching out for support, check the help pages provided by the team at https://docs.coscine.de/en/. This may have the information you need to solve the issue.
2. **Contact the team**: If the documentation does not help you or if you have a specific question, you can reach out to our support team at `servicedesk@itc.rwth-aachen.de` 📧. Provide a detailed description of the issue you're facing, including any error messages or screenshots if applicable.
3. **Be patient**: Our team will do their best to provide you with the support you need, but keep in mind that they may have a lot of requests to handle. Be patient and wait for their response.
4. **Provide feedback**: If the support provided by our support team helps you solve the issue, let us know! This will help us improve our documentation and support processes for future users.

By following these simple steps, you can get the support you need to use Coscine's services as an external user.

## 📦 Release & Changelog

External users can find the _Releases and Changelog_ inside each project's repository. The repository contains a section for Releases (`Deployments > Releases`), where users can find the latest release changelog and source. Withing the Changelog you can find a list of all the changes made in that particular release and version.  
By regularly checking for new releases and changes in the Changelog, you can stay up-to-date with the latest improvements and bug fixes by our team and community!


